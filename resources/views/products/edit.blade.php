@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Editar Producto<span class="float-right"><a href="/inventario" class="btn btn-block btn-warning btn">Cancelar</a></span></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="post" action="/inventario/{{ $producto->id }}">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label class="label">Código de barras</label>
                            <input required value="{{$producto->codigo_barras}}" autocomplete="off" name="codigo_barras" class="form-control"
                                   type="text" value="{{ $producto->codigo_barras }}">
                        </div>
                        <div class="form-group">
                            <label class="label">Descripción</label>
                            <input required value="{{$producto->descripcion}}" autocomplete="off" name="descripcion" class="form-control"
                                   type="text" value="{{$producto->descripcion}}">
                        </div>
                        <div class="form-group">
                            <label class="label">Precio de compra</label>
                            <input required value="{{$producto->precio_compra}}" autocomplete="off" name="precio_compra" class="form-control"
                                   type="decimal(9,2)" value="{{$producto->precio_compra}}">
                        </div>
                        <div class="form-group">
                            <label class="label">Precio de venta</label>
                            <input required value="{{$producto->precio_venta}}" autocomplete="off" name="precio_venta" class="form-control"
                                   type="decimal(9,2)" value="{{$producto->precio_venta}}">
                        </div>
                        <div class="form-group">
                            <label class="label">Existencia</label>
                            <input required value="{{$producto->existencia}}" autocomplete="off" name="existencia" class="form-control"
                                   type="decimal(9,2)" value="{{$producto->existencia}}">
                        </div>

                        <button type="submit"  name="submit" class="btn btn-info float-left">Editar</button>
                    </form>
                </div>
            </div>
        </div>

@endsection



