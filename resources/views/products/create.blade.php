@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Resgistrar Producto<span class="float-right"><a href="/inventario" class="btn btn-block btn-warning btn">Cancelar</a></span></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="post" action="/inventario">
                        @csrf
                        <div class="form-group">
                            <label class="label">Código de barras</label>
                            <input required autocomplete="off" name="codigo_barras" class="form-control"
                                   type="text" placeholder="Código de barras">
                        </div>
                        <div class="form-group">
                            <label class="label">Descripción</label>
                            <input required autocomplete="off" name="descripcion" class="form-control"
                                   type="text" placeholder="Descripción">
                        </div>
                        <div class="form-group">
                            <label class="label">Precio de compra</label>
                            <input required autocomplete="off" name="precio_compra" class="form-control"
                                   type="decimal(9,2)" placeholder="Precio de compra">
                        </div>
                        <div class="form-group">
                            <label class="label">Precio de venta</label>
                            <input required autocomplete="off" name="precio_venta" class="form-control"
                                   type="decimal(9,2)" placeholder="Precio de venta">
                        </div>
                        <div class="form-group">
                            <label class="label">Existencia</label>
                            <input required autocomplete="off" name="existencia" class="form-control"
                                   type="decimal(9,2)" placeholder="Existencia">
                        </div>

                        <button type="submit"  name="submit" class="btn btn-info float-left">Agregar</button>
                    </form>
                </div>
            </div>
        </div>

@endsection



