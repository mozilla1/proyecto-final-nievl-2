@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Resgistrar Cliente<span class="float-right"><a href="/clientes" class="btn btn-block btn-warning btn">Cancelar</a></span></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="post" action="/clientes/{{ $cliente->id }}">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label class="label">Nombre</label>
                            <input required value="{{$cliente->nombre}}" autocomplete="off" name="nombre" class="form-control"
                                   type="text" placeholder="Nombre">
                        </div>
                        <div class="form-group">
                            <label class="label">Teléfono</label>
                            <input required value="{{$cliente->telefono}}" autocomplete="off" name="telefono" class="form-control"
                                   type="text" placeholder="Teléfono">
                        </div>

                        <button type="submit"  name="submit" class="btn btn-info float-left">Agregar</button>
                    </form>
                </div>
            </div>
        </div>

@endsection



