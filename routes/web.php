<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/clientes', function () {
    return view('/clientes');
});

Route::get('/ventas', function () {
    return view('/ventas');
});

Route::get('/ventas_index', function () {
    return view('/ventas_index');
});

Route::get('/ventas_show', function () {
    return view('/ventas_show');
});

Route::get('/', function () {
    return view('welcome');
});

Route::get('/inventario', function () {
    return view('inventario');
});

Route::get('/products/create', function () {
    return view('products.create');
});

Route::get('/products/edit', function () {
    return view('products.edit');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware("auth")
    ->group(function () {
        Route::resource('clientes','ClientesController');
        Route::resource("usuarios", "UserController")->parameters(["usuarios" => "user"]);
        Route::resource('inventario','ProductosController');       
        Route::resource("/ventas_index", "VentasController");   
        Route::resource("/ventas_show", "VentasController");
        //Route::resource("/ventas_index", "VentasController@destroy");  
        //Route::get("/ventas", "VenderController@index")->name("ventas.index");
        //Route::get("/ventas", "VenderController@index")->name("ventas.show");
        Route::get("/ventas", "VenderController@index")->name("terminarOCancelarVenta");       
        Route::post("/productoDeVenta", "VenderController@agregarProductoVenta")->name("agregarProductoVenta");
        Route::delete("/productoDeVenta", "VenderController@quitarProductoDeVenta")->name("quitarProductoDeVenta");
        Route::post("/terminarOCancelarVenta", "VenderController@terminarOCancelarVenta")->name("terminarOCancelarVenta");
    });


